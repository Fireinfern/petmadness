﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        private static UIManager _instance;

        public static UIManager Instance => _instance;

        [SerializeField] private GameObject playerHUD;

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }

            _instance = this;
        }

        public void RegisterHUD(GameObject newPlayerHUD)
        {
            playerHUD = newPlayerHUD;
        }
        
        public void ShowHUD(GameObject possessedPawn)
        {
            if (!playerHUD) return;
            playerHUD.SetActive(true);
        }

        public void HideHUD()
        {
            if (!playerHUD) return;
            playerHUD.SetActive(false);
        }

        public void ShowEndLevelTransition()
        {
            SceneManager.LoadScene("TransitionLevel", LoadSceneMode.Additive);
        }
    }
}