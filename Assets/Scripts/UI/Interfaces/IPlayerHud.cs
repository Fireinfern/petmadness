﻿using UnityEngine;

namespace UI.Interfaces
{
    public interface IPlayerHud
    {
        public void RegisterPlayerCharacter(GameObject possessedPawn);
    }
}