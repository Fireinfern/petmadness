﻿using System;
using System.Globalization;
using Damage;
using GameMode;
using TMPro;
using UI.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthUIController : MonoBehaviour, IPlayerHud
    {
        [SerializeField] private Slider healthSlider;

        [SerializeField] private TMP_Text healthText;

        private float _maxHealth;
        
        private void OnUpdateHealth(float oldValue, float newValue)
        {
            healthSlider.value = newValue / _maxHealth;
            healthText.SetText(newValue.ToString(CultureInfo.CurrentCulture));
        }

        public void RegisterPlayerCharacter(GameObject possessedPawn)
        {
            HealthComponent healthComponent = possessedPawn.GetComponent<HealthComponent>();
            if (healthComponent)
            {
                healthComponent.healthChanged.AddListener(OnUpdateHealth);
                _maxHealth = healthComponent.MaxHealth;
                healthText.SetText(healthComponent.CurrentHealth.ToString(CultureInfo.CurrentCulture));
            }
        }
    }
}