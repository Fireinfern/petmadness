﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class TransitionLevelController : MonoBehaviour
    {

        private string _levelName = "MainMenu";

        public void RegisterNextLevelToOpen(string levelName)
        {
            _levelName = levelName;
        }
        
        public void OpenNextLevel()
        {
            SceneManager.LoadScene(_levelName);
        }
    }
}