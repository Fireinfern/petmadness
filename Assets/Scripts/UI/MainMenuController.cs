﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject settingsPanel;

        [SerializeField] private GameObject mainMenuPanel;

        public void OpenSettingsMenu()
        {
            mainMenuPanel.SetActive(false);
            settingsPanel.SetActive(true);
        }

        public void OpenMainMenu()
        {
            mainMenuPanel.SetActive(true);
            settingsPanel.SetActive(false);
        }

        public void StartNewGame()
        {
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}