﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class TryAgainController : MonoBehaviour
    {
        public void OnTryAgain()
        {
            SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        }

        public void OnExit()
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}