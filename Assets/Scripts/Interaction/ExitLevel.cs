﻿using System;
using GameManager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Interaction
{
    [RequireComponent(typeof(Collider2D))]
    public class ExitLevel : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }
            
            SceneManager.LoadScene(DungeonManager.Instance.GetNextDungeon(), LoadSceneMode.Single);
        }
    }
}