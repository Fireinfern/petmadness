﻿using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Interaction
{
    public class BossExit : ExitLevel
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player"))
            {
                return;
            }
            
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}