﻿using Damage;
using UnityEngine;

namespace Projectiles
{
    [CreateAssetMenu(fileName = "NewProjectile", menuName = "Projectiles", order = 1)]
    public class ProjectileType : ScriptableObject
    {
        [SerializeField]
        private DamageObject damageObject;

        public DamageObject DamageObject => damageObject;

        public Vector2 CalculateNextPosition(Vector2 currentPosition, Vector2 direction, float deltaTime)
        {
            Vector2 targetPosition = currentPosition + (direction * Speed);
            return Vector2.Lerp(currentPosition, targetPosition, deltaTime);
        }

        [SerializeField]
        private float speed;

        public float Speed => speed;
    }
}