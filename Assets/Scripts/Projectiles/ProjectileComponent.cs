﻿using System;
using Characters;
using Damage;
using Unity.VisualScripting;
using UnityEngine;

namespace Projectiles
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class ProjectileComponent : MonoBehaviour
    {
        [SerializeField]
        private ProjectileType projectileType;

        private Rigidbody2D _rigidbody2D;

        private GameObject _ownerGameObject;

        private void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public void SetOwnerGameObject(GameObject newOwner)
        {
            _ownerGameObject = newOwner;
        }
        
        private void Update()
        {
            if (!_rigidbody2D) return;
            _rigidbody2D.velocity = transform.right * projectileType.Speed;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GameObject() == _ownerGameObject) return;
            HealthComponent healthComponent = other.GetComponent<HealthComponent>();
            if (!healthComponent)
            {
                Destroy(gameObject);
                return;
            }
            healthComponent.IReceiveDamage(projectileType.DamageObject.DamageAmount);
            Destroy(gameObject);
        }
    }
}