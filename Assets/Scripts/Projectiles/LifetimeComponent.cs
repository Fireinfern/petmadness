﻿using System;
using UnityEngine;

namespace Projectiles
{
    // This Component will be changed for a polling system
    public class LifetimeComponent : MonoBehaviour
    {
        [SerializeField]
        private float timeToLive = 7.0f;

        private void Update()
        {
            timeToLive -= Time.deltaTime;
            if (timeToLive <= 0.0f)
            {
                Destroy(gameObject);
            }
        }
    }
}