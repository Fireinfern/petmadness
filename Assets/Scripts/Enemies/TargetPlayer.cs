﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Enemies
{
    public class TargetPlayer : MonoBehaviour
    {
        private Animator _animator;

        private GameObject _playerCharacter;

        public GameObject PlayerCharacter => _playerCharacter;
        
        public UnityEvent<Transform> onPlayerDetected;

        public UnityEvent onLoosePlayer;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player")) return;
            onPlayerDetected.Invoke(other.transform);
            _playerCharacter = other.gameObject;
            _animator.SetBool("PlayerDetected", true);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            onLoosePlayer.Invoke();
            _playerCharacter = null;
            _animator.SetBool("PlayerDetected", false);
        }
    }
}