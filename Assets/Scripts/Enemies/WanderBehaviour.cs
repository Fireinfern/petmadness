﻿using UnityEngine;

namespace Enemies
{
    public class WanderBehaviour : StateMachineBehaviour
    {
        private float _currentAngle = 0;
        private Vector2 _endPosition = Vector2.zero;

        [SerializeField]
        private float speed = 100;

        [SerializeField]
        private float distance = 20.0f;
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            _currentAngle = Random.Range(0, 360);
            _endPosition = animator.gameObject.transform.position;
            float inputAngleRadians = _currentAngle * Mathf.Deg2Rad;
            _endPosition += (new Vector2(Mathf.Cos(inputAngleRadians), Mathf.Sin(inputAngleRadians))) * distance;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            float remainingDistance = ((Vector2)animator.transform.position - _endPosition).sqrMagnitude;
            if (remainingDistance <= 0.1f)
            {
                animator.SetBool("isIdle", true);
                return;
            }
            Vector2 newPosition = Vector2.MoveTowards(animator.gameObject.transform.position, _endPosition,
                speed * Time.deltaTime);
            animator.GetComponent<Rigidbody2D>().position = newPosition;
        }

        public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }

        public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }
    }
}