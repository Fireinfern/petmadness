﻿using System;
using UnityEngine;

namespace Enemies
{
    public class EnemyDeathComponent : MonoBehaviour
    {
        private CapsuleCollider2D _capsuleCollider2D;

        private void Start()
        {
            _capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        }

        public void OnEnemyDie()
        {
            _capsuleCollider2D.enabled = false;
            Destroy(gameObject);
        }
    }
}