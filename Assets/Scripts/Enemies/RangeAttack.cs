﻿using System;
using UnityEngine;

namespace Enemies
{
    public class RangeAttack : MonoBehaviour
    {
        private Transform _targetTransform;

        [SerializeField]
        private float maxRange = 3.0f;

        private AttackTimer _attackTimer;
        
        private void Start()
        {
            _attackTimer = GetComponent<AttackTimer>();
        }

        public void UpdateTarget(Transform targetTransform)
        {
            _targetTransform = targetTransform;
        }

        public void ClearTarget()
        {
            _targetTransform = null;
        }

        public void Update()
        {
            if (_attackTimer.IsActive || !_targetTransform) return;
            float currentDistance = Vector3.Distance(_targetTransform.position, transform.position);
            if (currentDistance > maxRange) return;
            _attackTimer.StartAttack();
        }
    }
}