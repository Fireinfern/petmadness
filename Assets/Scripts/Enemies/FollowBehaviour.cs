﻿using UnityEngine;

namespace Enemies
{
    public class FollowBehaviour : StateMachineBehaviour
    {

        private Transform _target;

        [SerializeField]
        private float speed = 50.0f;

        [SerializeField]
        private float range = 20.0f;
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            _target = animator.GetComponent<TargetPlayer>().PlayerCharacter.transform;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            _target = null;
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            Vector2 direction = (Vector2) _target.position - (Vector2) animator.transform.position;
            float remainingDistance = direction.sqrMagnitude;
            if (remainingDistance <= range)
            {
                return;
            }
            Debug.Log(direction.normalized);
            animator.GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;
        }

        public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }

        public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }
    }
}