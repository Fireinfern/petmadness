﻿using UnityEngine;

namespace Enemies
{
    public class IdleBehaviour : StateMachineBehaviour
    {
        [SerializeField]
        private float timeWaiting = 2.0f;

        private float _remainingTime = 0;
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            _remainingTime = timeWaiting;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            _remainingTime -= Time.deltaTime;
            if (_remainingTime <= 0.0f)
            {
                animator.SetBool("isIdle", false);
            }
        }

        public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }

        public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }
    }
}