﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Enemies
{
    public class AttackTimer : MonoBehaviour
    {
        [SerializeField]
        private float timeBetweenAttacks = 5.0f;

        private bool _isActive = false;

        public bool IsActive => _isActive;

        public UnityEvent<Vector2> onAttack;

        private TargetPlayer _targetPlayerComponent;
        
        private void Start()
        {
            _targetPlayerComponent = GetComponent<TargetPlayer>();
        }

        public void StartAttack()
        {
            if (_isActive) return;
            _isActive = true;
            StartCoroutine(AttackCoroutine());
        }

        private IEnumerator AttackCoroutine()
        {
            while (_isActive)
            {
                Vector2 direction = (Vector2)_targetPlayerComponent.PlayerCharacter.transform.position;
                direction -= (Vector2)transform.position;
                onAttack.Invoke(direction.normalized);
                yield return new WaitForSeconds(timeBetweenAttacks);
            }
        }
    }
}