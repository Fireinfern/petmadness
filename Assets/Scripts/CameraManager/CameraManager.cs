﻿using System;
using Cinemachine;
using UnityEngine;

namespace CameraManager
{
    public class CameraManager : MonoBehaviour
    {
        public static CameraManager Instance { get; private set; }

        [SerializeField]
        private CinemachineVirtualCamera virtualCamera;

        public CinemachineVirtualCamera VirtualCamera => virtualCamera;
        
        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
                return;
            }

            Instance = this;
        }

        public void SetFollowCharacter(GameObject followingObject)
        {
            if (!virtualCamera) return;
            virtualCamera.Follow = followingObject.transform;
        }
    }
}