﻿using UnityEngine;

namespace Damage
{
    [CreateAssetMenu(fileName = "DamageObject", menuName = "Damage/BaseDamageObject", order = 0)]
    public class DamageObject : ScriptableObject
    {
        [SerializeField] private EDamageType type = EDamageType.None;

        public EDamageType Type => type;

        [SerializeField] private float damageAmount = 10.0f;

        public float DamageAmount => damageAmount;
    }
}