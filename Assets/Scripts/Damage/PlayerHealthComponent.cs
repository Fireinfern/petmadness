﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Damage
{
    public class PlayerHealthComponent : HealthComponent
    {
        private void Start()
        {
            characterDied.AddListener(OnCharacterDied);
        }

        private void OnCharacterDied()
        {
            SceneManager.LoadScene("LostMenu", LoadSceneMode.Additive);
        }
    }
}