﻿using UnityEngine;

namespace Damage
{
    [CreateAssetMenu(fileName = "DefaultPushBack", menuName = "Damage/Effects", order = 0)]
    public class PushBackEffect : DamageEffect
    {

        [SerializeField]
        private float pushForce = 10.0f;
        
        public override void ExecuteEffect(GameObject target, GameObject instigator)
        {
            Vector2 force = instigator.transform.position - target.transform.position;
            force.Normalize();
            force *= pushForce;
            target.GetComponent<Rigidbody2D>()?.AddForce(force, ForceMode2D.Impulse);
        }
    }
}