﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Damage
{
    public class SpikeComponent : HazardComponent
    {
        private List<HealthComponent> _healthComponents;

        private bool _isActive;

        [SerializeField] private float timeForDamage = 2.0f;

        private void Start()
        {
            _healthComponents = new List<HealthComponent>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            HealthComponent healthComponent = other.GetComponent<HealthComponent>();
            if (!healthComponent) return;
            _healthComponents.Add(healthComponent);
            if (!_isActive)
            {
                _isActive = true;
                StartCoroutine(DamageOutput());
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            HealthComponent healthComponent = other.GetComponent<HealthComponent>();
            if (!healthComponent) return;
            _healthComponents.Remove(healthComponent);
        }

        IEnumerator DamageOutput()
        {
            while (_isActive)
            {
                yield return new WaitForSeconds(timeForDamage);
                if (_healthComponents.Count <= 0) _isActive = false;
                foreach (var healthComponent in _healthComponents.Where(healthComponent => healthComponent))
                {
                    ExecuteDamage(healthComponent);
                }
            }
        }
    }
}