﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameManager
{
    public class DungeonManager : MonoBehaviour
    {
        public static DungeonManager Instance { get; private set; }

        [SerializeField] private List<String> possibleDungeonNames;

        private int _dungeonCount = 0;

        public int DungeonCount => _dungeonCount;

        [SerializeField]
        private AudioSource musicSource;
        
        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad(this);
                return;
            }

            if (Instance != this)
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            musicSource.Play();
        }

        public String GetNextDungeon()
        {
            if (_dungeonCount >= 3)
            {
                _dungeonCount = 0;
                return "BossBattle";
            }
            ++_dungeonCount;
            return possibleDungeonNames[Random.Range(0, possibleDungeonNames.Count)];
        }
    }
}