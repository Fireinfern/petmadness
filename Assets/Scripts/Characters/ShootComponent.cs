﻿using Projectiles;
using UnityEngine;

namespace Characters
{
    public class ShootComponent : MonoBehaviour
    {
        [SerializeField] private GameObject projectileBase;
        
        public void ShootProjectile(Vector2 direction)
        {
            Vector3 positionToSpawnIn = transform.position;
            positionToSpawnIn += (Vector3)(direction);
            Quaternion rotation = Quaternion.LookRotation(Vector3.forward, new Vector3(-direction.y, direction.x, 0.0f));
            GameObject projectile = Instantiate(projectileBase, positionToSpawnIn, rotation);
            projectile.GetComponent<ProjectileComponent>()?.SetOwnerGameObject(gameObject);
        }
    }
}