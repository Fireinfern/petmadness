﻿using System;
using UnityEngine;

namespace Characters
{
    public class CharacterAnimation : MonoBehaviour
    {
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;
        private static readonly int IsRunning = Animator.StringToHash("IsRunning");

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void SetRunningState(bool isRunning)
        {
            _animator.SetBool(IsRunning, isRunning);
        }

        public void UpdateVelocity(Vector2 characterVelocity)
        {
            if (characterVelocity.Equals(Vector2.zero))
            {
                SetRunningState(false);
                return;
            }
            SetRunningState(true);
            if (characterVelocity.x > 0)
            {
                _spriteRenderer.flipX = false;
                return;
            }

            if (characterVelocity.x < 0)
            {
                _spriteRenderer.flipX = true;
            }
        }
    }
}