﻿using System;
using UnityEngine;

namespace Characters
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class MovementComponent : MonoBehaviour
    {

        private Rigidbody2D _rigidbody2D;

        private CharacterAnimation _characterAnimation;
        
        [SerializeField]
        private float speed = 10.0f;

        private void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _characterAnimation = GetComponent<CharacterAnimation>();
        }

        public void AddInput(Vector2 movementInput)
        {
            if (!_rigidbody2D) return;
            _rigidbody2D.velocity = speed * movementInput;
            _characterAnimation.UpdateVelocity(_rigidbody2D.velocity);
        }
    }
}