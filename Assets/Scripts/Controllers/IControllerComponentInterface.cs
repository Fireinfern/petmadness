﻿using UnityEngine;

namespace Controllers
{
    public interface IControllerComponentInterface
    {
        void IOnPosses(GameObject possessedObject);

        void IOnUnPossess();
    }
}