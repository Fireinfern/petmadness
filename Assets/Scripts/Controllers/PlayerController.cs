﻿using UI;
using UnityEngine;

namespace Controllers
{
    public class PlayerController : MonoBehaviour, IControllerInterface
    {
        private GameObject _controlledCharacter;
        
        public void IPossess(GameObject character)
        {
            if (character == null) return;
            _controlledCharacter = character;
            var controllerComponents = GetComponents<IControllerComponentInterface>();
            CameraManager.CameraManager.Instance.SetFollowCharacter(character);
            foreach (var component in controllerComponents)
            {
                component.IOnPosses(_controlledCharacter);
            }
            UIManager.Instance.ShowHUD(_controlledCharacter);
        }

        public void IUnPossess()
        {
            _controlledCharacter = null;
            var controllerComponents = GetComponents<IControllerComponentInterface>();
            foreach (var component in controllerComponents)
            {
                component.IOnUnPossess();
            }
        }

        public GameObject IGetPossessCharacter()
        {
            return _controlledCharacter;
        }
    }
}