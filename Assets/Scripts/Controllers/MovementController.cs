﻿using System;
using Characters;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    [RequireComponent(typeof(PlayerInput))]
    public class MovementController : MonoBehaviour, IInputBindedInterface, IControllerComponentInterface
    {
        private Vector2 _movementVector = Vector2.zero;

        private GameObject _possessedObject;

        private MovementComponent _possessedMovementComponent;

        public void ISetupInputBindings(PlayerInput inputComponent)
        {
            if (inputComponent == null) return;
            InputAction action = inputComponent.actions.FindAction("Move", true);
            action.performed += MoveActionPerformed;
            action.canceled += MoveActionCanceled;
        }

        void MoveActionPerformed(InputAction.CallbackContext context)
        {
            _movementVector = context.ReadValue<Vector2>();
            _movementVector.Normalize();
        }

        void MoveActionCanceled(InputAction.CallbackContext context)
        {
            _movementVector = context.ReadValue<Vector2>();
            _movementVector.Normalize();
        }

        private void FixedUpdate()
        {
            if (_possessedMovementComponent)
            {
                _possessedMovementComponent.AddInput(_movementVector);
            }
        }

        public void IOnPosses(GameObject possessedObject)
        {
            if (possessedObject == null) return;
            _possessedObject = possessedObject;
            _possessedMovementComponent = _possessedObject.GetComponent<MovementComponent>();
        }

        public void IOnUnPossess()
        {
            _possessedObject = null;
            _possessedMovementComponent = null;
        }
    }
}