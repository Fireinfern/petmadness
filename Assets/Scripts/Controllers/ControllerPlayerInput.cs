﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    public class ControllerPlayerInput : PlayerInput
    {
        private void Start()
        {
            IInputBindedInterface[] Components = GetComponents<IInputBindedInterface>();
            foreach (var inputComponent in Components)
            {
                inputComponent.ISetupInputBindings(this);
            }
        }
    }
}