﻿using UI.Interfaces;
using UnityEngine;

namespace Controllers
{
    public class UIController : MonoBehaviour, IControllerComponentInterface
    {
        [SerializeField] private GameObject playerHUD;

        private GameObject _instancedHUD;

        public GameObject InstancedHUD => _instancedHUD;
        
        public void IOnPosses(GameObject possessedObject)
        {
            if (!playerHUD) return;
            if (!_instancedHUD)
            {
                _instancedHUD = Instantiate(playerHUD);
            }
            _instancedHUD.SetActive(true);
            IPlayerHud[] hudComponents = _instancedHUD.GetComponents<IPlayerHud>();
            foreach (var component in hudComponents)
            {
                component.RegisterPlayerCharacter(possessedObject);
            }
        }

        public void IOnUnPossess()
        {
            if (!_instancedHUD) return;
            _instancedHUD.SetActive(false);
        }
    }
}