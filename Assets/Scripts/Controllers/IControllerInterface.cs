﻿using UnityEngine;

namespace Controllers
{
    public interface IControllerInterface
    {
        public void IPossess(GameObject character);

        public void IUnPossess();

        public GameObject IGetPossessCharacter();
    }
}