﻿using Characters;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controllers
{
    public class ShootController : MonoBehaviour, IInputBindedInterface, IControllerComponentInterface
    {
        
        private ShootComponent _possessedShootComponent;
        
        public void ISetupInputBindings(PlayerInput inputComponent)
        {
            if (inputComponent == null) return;
            InputAction action = inputComponent.actions.FindAction("Fire", true);
            action.performed += FireInputTriggered;
        }

        public void IOnPosses(GameObject possessedObject)
        {
            _possessedShootComponent = possessedObject.GetComponent<ShootComponent>();
        }

        public void IOnUnPossess()
        {
            _possessedShootComponent = null;
        }

        void FireInputTriggered(InputAction.CallbackContext context)
        {
            if (!_possessedShootComponent)
            {
                return;
            }
            
            Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction -= (Vector2)(_possessedShootComponent.transform.position); 
            direction.Normalize();
            _possessedShootComponent.ShootProjectile(direction);
        }
    }
}