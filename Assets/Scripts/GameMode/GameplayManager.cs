﻿using System;
using System.Collections.Generic;
using Controllers;
using Registry;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameMode
{
    public class GameplayManager : MonoBehaviour
    {
        static public GameplayManager Instance { get; private set; }

        [SerializeField] private GameMode gameMode;

        public GameMode GameMode => gameMode;

        private GameObject _playerControllerInstance;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
                return;
            }

            Instance = this;
        }

        private void Start()
        {
            if (GameMode == null || !GameMode.IsValid()) return;
            
            LevelRegistrySubsystem levelRegistrySubsystem = LevelRegistrySubsystem.Instance;
            PlayerStart.PlayerStart[] playerStarts= Object.FindObjectsOfType<PlayerStart.PlayerStart>();
            if (playerStarts.Length < 1) return;
            PlayerStart.PlayerStart selectedStart = playerStarts[0];
            foreach (var start in playerStarts)
            {
                if (start.PlayerStartName.Equals(GameMode.PlayerStartName))
                {
                    selectedStart = start;
                    break;
                }
            }

            _playerControllerInstance = Instantiate(GameMode.ControllerPrefab, selectedStart.transform.position, selectedStart.transform.rotation);
            GameObject character = Instantiate(GameMode.CharacterPrefab, selectedStart.transform.position, selectedStart.transform.rotation);
            _playerControllerInstance.GetComponent<IControllerInterface>()?.IPossess(character);
        }
    }
}