﻿using UnityEngine;

namespace GameMode
{
    [CreateAssetMenu(fileName = "NewGameMode", menuName = "GameMode", order = 0)]
    public class GameMode : ScriptableObject
    {
        [SerializeField] private string playerStartName = "defaultStart";

        [SerializeField] private GameObject characterPrefab;

        [SerializeField] private GameObject controllerPrefab;

        public string PlayerStartName => playerStartName;

        public GameObject CharacterPrefab => characterPrefab;

        public GameObject ControllerPrefab => controllerPrefab;

        public bool IsValid()
        {
            return characterPrefab != null && controllerPrefab != null;
        }
    }
}