﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Registry
{
    public class LevelRegistrySubsystem : MonoBehaviour
    {
        public static LevelRegistrySubsystem Instance { get; private set; }

        private Dictionary<Type, List<MonoBehaviour>> _registeredComponents = new Dictionary<Type, List<MonoBehaviour>>();

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        public void RegisterComponent(MonoBehaviour component)
        {
            if (_registeredComponents.ContainsKey(component.GetType()) &&
                _registeredComponents[component.GetType()].Contains(component)) return;
            if (!_registeredComponents.ContainsKey(component.GetType()))
            {
                _registeredComponents.Add(component.GetType(), new List<MonoBehaviour>());
            }
            _registeredComponents[component.GetType()].Add(component);
        }

        public List<T> GetRegisteredComponentsOfType<T>() where T : MonoBehaviour
        {
            List<T> returnList = new List<T>();
            if (!_registeredComponents.ContainsKey(typeof(T))) return returnList;
            foreach (var component in _registeredComponents[typeof(T)])
            {
                returnList.Add((T) (component));
            }
            return returnList;
        }
    }
}